"""This module use for testing simple CI/CD """
import math


def get_hypotenuse(side_1, side_2):
  
    return math.sqrt(side_1 * side_1 + side_2 * side_2)


def get_area(side_1, side_2):
    """
    calc area with formula (a * b) / 2
    :param side_1: side a of right angle triangle
    :param side_2: side b of right angle triangle
    :return: area of right angle triangle
    """
    return side_1 * side_2 / 2


if __name__ == "main":
    a = int(input("Input a:"))
    b = int(input("Input b:"))
    print("c = ", get_hypotenuse(a, b))
    print("S = ", get_area(a, b))
